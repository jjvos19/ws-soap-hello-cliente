
package bo.com.fie.cognos.cursojava.cliente.dl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the bo.com.fie.cognos.cursojava.cliente.dl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Saludar_QNAME = new QName("http://services.cursojava.cognos.fie.com.bo/", "saludar");
    private final static QName _ObtenerPersonaResponse_QNAME = new QName("http://services.cursojava.cognos.fie.com.bo/", "obtenerPersonaResponse");
    private final static QName _SaludarResponse_QNAME = new QName("http://services.cursojava.cognos.fie.com.bo/", "saludarResponse");
    private final static QName _ObtenerPersona_QNAME = new QName("http://services.cursojava.cognos.fie.com.bo/", "obtenerPersona");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: bo.com.fie.cognos.cursojava.cliente.dl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Saludar }
     * 
     */
    public Saludar createSaludar() {
        return new Saludar();
    }

    /**
     * Create an instance of {@link ObtenerPersonaResponse }
     * 
     */
    public ObtenerPersonaResponse createObtenerPersonaResponse() {
        return new ObtenerPersonaResponse();
    }

    /**
     * Create an instance of {@link SaludarResponse }
     * 
     */
    public SaludarResponse createSaludarResponse() {
        return new SaludarResponse();
    }

    /**
     * Create an instance of {@link ObtenerPersona }
     * 
     */
    public ObtenerPersona createObtenerPersona() {
        return new ObtenerPersona();
    }

    /**
     * Create an instance of {@link Persona }
     * 
     */
    public Persona createPersona() {
        return new Persona();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Saludar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com.bo/", name = "saludar")
    public JAXBElement<Saludar> createSaludar(Saludar value) {
        return new JAXBElement<Saludar>(_Saludar_QNAME, Saludar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerPersonaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com.bo/", name = "obtenerPersonaResponse")
    public JAXBElement<ObtenerPersonaResponse> createObtenerPersonaResponse(ObtenerPersonaResponse value) {
        return new JAXBElement<ObtenerPersonaResponse>(_ObtenerPersonaResponse_QNAME, ObtenerPersonaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaludarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com.bo/", name = "saludarResponse")
    public JAXBElement<SaludarResponse> createSaludarResponse(SaludarResponse value) {
        return new JAXBElement<SaludarResponse>(_SaludarResponse_QNAME, SaludarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerPersona }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com.bo/", name = "obtenerPersona")
    public JAXBElement<ObtenerPersona> createObtenerPersona(ObtenerPersona value) {
        return new JAXBElement<ObtenerPersona>(_ObtenerPersona_QNAME, ObtenerPersona.class, null, value);
    }

}
