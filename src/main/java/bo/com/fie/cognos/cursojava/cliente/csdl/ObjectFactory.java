
package bo.com.fie.cognos.cursojava.cliente.csdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the bo.com.fie.cognos.cursojava.cliente.csdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CargarPersonas_QNAME = new QName("http://services.cursojava.cognos.fie.com.bo/", "cargarPersonas");
    private final static QName _CargarPersonasRResponse_QNAME = new QName("http://services.cursojava.cognos.fie.com.bo/", "cargarPersonasRResponse");
    private final static QName _Saludar_QNAME = new QName("http://services.cursojava.cognos.fie.com.bo/", "saludar");
    private final static QName _CargarPersonasResponse_QNAME = new QName("http://services.cursojava.cognos.fie.com.bo/", "cargarPersonasResponse");
    private final static QName _CargarPersonasR_QNAME = new QName("http://services.cursojava.cognos.fie.com.bo/", "cargarPersonasR");
    private final static QName _SaludarResponse_QNAME = new QName("http://services.cursojava.cognos.fie.com.bo/", "saludarResponse");
    private final static QName _Exception_QNAME = new QName("http://services.cursojava.cognos.fie.com.bo/", "Exception");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: bo.com.fie.cognos.cursojava.cliente.csdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CargarPersonas }
     * 
     */
    public CargarPersonas createCargarPersonas() {
        return new CargarPersonas();
    }

    /**
     * Create an instance of {@link CargarPersonasRResponse }
     * 
     */
    public CargarPersonasRResponse createCargarPersonasRResponse() {
        return new CargarPersonasRResponse();
    }

    /**
     * Create an instance of {@link Saludar }
     * 
     */
    public Saludar createSaludar() {
        return new Saludar();
    }

    /**
     * Create an instance of {@link CargarPersonasResponse }
     * 
     */
    public CargarPersonasResponse createCargarPersonasResponse() {
        return new CargarPersonasResponse();
    }

    /**
     * Create an instance of {@link CargarPersonasR }
     * 
     */
    public CargarPersonasR createCargarPersonasR() {
        return new CargarPersonasR();
    }

    /**
     * Create an instance of {@link SaludarResponse }
     * 
     */
    public SaludarResponse createSaludarResponse() {
        return new SaludarResponse();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link Persona }
     * 
     */
    public Persona createPersona() {
        return new Persona();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargarPersonas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com.bo/", name = "cargarPersonas")
    public JAXBElement<CargarPersonas> createCargarPersonas(CargarPersonas value) {
        return new JAXBElement<CargarPersonas>(_CargarPersonas_QNAME, CargarPersonas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargarPersonasRResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com.bo/", name = "cargarPersonasRResponse")
    public JAXBElement<CargarPersonasRResponse> createCargarPersonasRResponse(CargarPersonasRResponse value) {
        return new JAXBElement<CargarPersonasRResponse>(_CargarPersonasRResponse_QNAME, CargarPersonasRResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Saludar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com.bo/", name = "saludar")
    public JAXBElement<Saludar> createSaludar(Saludar value) {
        return new JAXBElement<Saludar>(_Saludar_QNAME, Saludar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargarPersonasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com.bo/", name = "cargarPersonasResponse")
    public JAXBElement<CargarPersonasResponse> createCargarPersonasResponse(CargarPersonasResponse value) {
        return new JAXBElement<CargarPersonasResponse>(_CargarPersonasResponse_QNAME, CargarPersonasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargarPersonasR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com.bo/", name = "cargarPersonasR")
    public JAXBElement<CargarPersonasR> createCargarPersonasR(CargarPersonasR value) {
        return new JAXBElement<CargarPersonasR>(_CargarPersonasR_QNAME, CargarPersonasR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaludarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com.bo/", name = "saludarResponse")
    public JAXBElement<SaludarResponse> createSaludarResponse(SaludarResponse value) {
        return new JAXBElement<SaludarResponse>(_SaludarResponse_QNAME, SaludarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com.bo/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

}
