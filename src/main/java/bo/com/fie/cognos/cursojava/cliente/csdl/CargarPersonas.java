
package bo.com.fie.cognos.cursojava.cliente.csdl;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para cargarPersonas complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="cargarPersonas">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personas" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="separador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cargarPersonas", propOrder = {
    "personas",
    "separador"
})
public class CargarPersonas {

    protected List<String> personas;
    protected String separador;

    /**
     * Gets the value of the personas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the personas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPersonas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPersonas() {
        if (personas == null) {
            personas = new ArrayList<String>();
        }
        return this.personas;
    }

    /**
     * Obtiene el valor de la propiedad separador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeparador() {
        return separador;
    }

    /**
     * Define el valor de la propiedad separador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeparador(String value) {
        this.separador = value;
    }

}
