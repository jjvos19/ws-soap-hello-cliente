package bo.com.fie.cognos.cursojava.cliente;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

import bo.com.fie.cognos.cursojava.cliente.csdl.ClientesDLServices;
import bo.com.fie.cognos.cursojava.cliente.csdl.ClientesDLServicesService;
import bo.com.fie.cognos.cursojava.cliente.csdl.Exception_Exception;
//import bo.com.fie.cognos.cursojava.cliente.csdl.ClientesDLServices;
//import bo.com.fie.cognos.cursojava.cliente.csdl.ClientesDLServicesService;
import bo.com.fie.cognos.cursojava.cliente.rl.HolaMundoService;
import bo.com.fie.cognos.cursojava.cliente.rl.HolaMundoServiceService;
import bo.com.fie.cognos.cursojava.cliente.rl.Persona;

public class ClienteHolaMundo {

	// Cliente para para el servicio.
	public static void main(String args[]) {
		clienteRL();
		clienteClienteDL();
		clienteClienteDLSaludar();
		clienteRemotoSaludar();
	}

	private static void clienteRL() {
		HolaMundoServiceService servicio = new HolaMundoServiceService();
		HolaMundoService cliente = servicio.getHolaMundoServicePort();
		String saludo = cliente.saludar();
		System.out.println(saludo);
		short edad = 32;
		Persona p = cliente.obtenerPersona("JJ", "Val", "Or", edad);
		System.out.println(p.getNombre() + " " + p.getApPaterno() + " " + p.getApMaterno() + ", " + p.getEdad());
	}

	/*
	 * TAREA Crear un web services que permita la creacion de un listado de personas
	 * y un cliente que muestre las personas por consola. Nombre apPaterno apMaterno
	 * Edad
	 */
	private static void clienteClienteDL() {

		ClientesDLServicesService servicio = new ClientesDLServicesService();
		ClientesDLServices cliente = servicio.getClientesDLServicesPort();
		List<String> lista = new LinkedList();
		String separador = ",";
		for (int i = 0; i < 10; i++) {
			int j = i + 1;
			String cadena = "nombre" + j + separador + "paterno" + j + separador + "materno" + j + separador + (j + 10);
			lista.add(cadena);
		}
		List<bo.com.fie.cognos.cursojava.cliente.csdl.Persona> listaPersonas = cliente.cargarPersonas(lista, separador);
		for (bo.com.fie.cognos.cursojava.cliente.csdl.Persona p : listaPersonas) {
			System.out.println(
					String.format("%s %s %s, %d", p.getNombre(), p.getApPaterno(), p.getApMaterno(), p.getEdad()));
		}

		listaPersonas = cliente.cargarPersonasR(50);
		for (bo.com.fie.cognos.cursojava.cliente.csdl.Persona p : listaPersonas) {
			System.out.println(
					String.format("%s %s %s, %d", p.getNombre(), p.getApPaterno(), p.getApMaterno(), p.getEdad()));
		}
	}

	public static void clienteClienteDLSaludar() {
		ClientesDLServicesService servicio = new ClientesDLServicesService();
		ClientesDLServices cliente = servicio.getClientesDLServicesPort();
		Map<String, Object> mapRequest = ((BindingProvider) cliente).getRequestContext();
		Map<String, List<String>> headers = new HashMap();
		headers.put("usuario", java.util.Collections.singletonList("jj"));
		headers.put("password", java.util.Collections.singletonList("JVO9"));
		mapRequest.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
		try {
			System.out.println(cliente.saludar());
		} catch (Exception_Exception e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
		}
	}

	public static void clienteRemotoSaludar() {
		bo.com.fie.cognos.cursojava.cliente.csRemota.HolaMundoServiceService servicio = new bo.com.fie.cognos.cursojava.cliente.csRemota.HolaMundoServiceService();
		bo.com.fie.cognos.cursojava.cliente.csRemota.HolaMundoService cliente = servicio.getHolaMundoServicePort();
		try {
			Map<String, Object> mapRequest = ((BindingProvider) cliente).getRequestContext();
			Map<String, List<String>> headers = new HashMap();
			headers.put("usuario", java.util.Collections.singletonList("jj"));
			headers.put("password", java.util.Collections.singletonList("JVO9"));
			mapRequest.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
			cliente.saludar("hi");
		} catch (bo.com.fie.cognos.cursojava.cliente.csRemota.Exception_Exception e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
		}
	}

	/*
	 * private static void clienteRE() { HolaMundoREServiceService servicio = new
	 * HolaMundoREServiceService();
	 * 
	 * bo.com.fie.cognos.cursojava.cliente.re.HolaMundoREService cliente =
	 * servicio.getHolaMundoREServicePort(); String saludo = cliente.saludar();
	 * System.out.println(saludo); short edad = 32; Persona p =
	 * cliente.obtenerPersona("Juan Jose", "Valencia", "Oropeza", edad);
	 * System.out.println(p.getNombre() + " " + p.getApPaterno() + " " +
	 * p.getApMaterno() + ", " + p.getEdad()); }
	 */
}
